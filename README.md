# ExWeb3

Helpers and utility functions to work with keccak256, secp256k1 and other low-level primitives used on EVM-compatiable networks, like: Ethereum, Polygon, Binance Smart Chain etc.

For API documentations, check [hex](https://hexdocs.pm/ex_web3/ExWeb3.html).

ExWeb3 can be easily integrated with Phoenix and [Guardian](https://github.com/ueberauth/guardian) to authenticate requests signed by EVM-compatiable wallet like Metamask with the help of `ExWeb3.Guardian.Plug.VerifySignature` and `ExWeb3.Phoenix.Plug.SaveRawBody` plugs.

Just add `plug ExWeb3.Phoenix.Plug.SaveRawBody, match_req_path: ~r[^(.*)$]` to `endpoint.ex` before `plug Plug.Parsers` to allow signature authentication for all routes, then you can use Guardian plug in router pipelines normally:

```elixir
plug ExWeb3.Guardian.Plug.VerifySignature,
  resource_by_pub_key: &Filters.fetch_by_pub_key/1,
  claims_for_resource: &(MyApp.ImplementationModule.build_claims(%{}, &1))
```

## Installation

Add `ex_web3` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ex_web3, "~> 0.2.0"}
  ]
end
```
