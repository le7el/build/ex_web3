if Code.ensure_loaded?(Plug) do
  defmodule ExWeb3.Phoenix.Plug.SaveRawBody do
    @moduledoc """
    Phoenix decodes original body message in Endpoint,
    so to get proper digest, we need to preserve original message,
    before Plug.Parsers are called.

    Put this plug just before Plug.Parsers in endpoint.ex of your
    Phoenix app.
    """
    @behaviour Plug

    @impl Plug
    @spec init(opts :: Keyword.t()) :: Keyword.t()
    def init(opts \\ []) do
      unless Keyword.has_key?(opts, :match_req_path),
        do: throw "Regular expression :match_req_path is required"

      opts
    end

    @impl Plug
    @spec call(Plug.Conn.t(), Keyword.t()) :: Plug.Conn.t()
    def call(conn, opts) do
      mask = Keyword.fetch!(opts, :match_req_path)
      case String.match?(conn.request_path, mask) do
        false ->
          conn

        true ->
          {:ok, body, _} = Plug.Conn.read_body(conn)
          Plug.Conn.put_private(conn, :raw_body, body)
      end
    end
  end
end
