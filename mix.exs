defmodule ExWeb3.MixProject do
  use Mix.Project

  def project do
    [
      app: :ex_web3,
      version: "0.2.3",
      elixir: "~> 1.12",
      description: "Utility functions for Elixir to simplify working with secp256k1 elliptic curve and EVM-based networks.",
      docs: [extras: ["README.md"]],
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      package: package(),
      elixirc_paths: elixirc_paths(Mix.env()),
      deps: deps(),

      # Docs
      name: "ExWeb3",
      source_url: "https://gitlab.com/le7el/build/ex_web3",
      docs: [
        main: "ExWeb3",
        extras: ["README.md"]
      ]
    ]
  end

  def package do
    [
      name: :ex_web3,
      files: ["lib", "mix.exs"],
      maintainers: ["LE7EL DAO"],
      licenses: ["MPL-2.0"],
      links: %{"Gitlab" => "https://gitlab.com/le7el/build/ex_web3"}
    ]
  end

  def application do
    [
      extra_applications: [:logger, :plug]
    ]
  end

  defp deps do
    [
      {:guardian, "~> 2.2.0"},
      {:ex_keccak, "~> 0.7"},
      {:ex_secp256k1, "~> 0.7"},
      {:ex_abi, "~> 0.6"},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:jason, "~> 1.4"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/stub_modules"]
  defp elixirc_paths(_), do: ["lib"]
end
